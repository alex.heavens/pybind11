FROM ubuntu:20.04

USER root

# Interactive mode requires user-prompts during build.
RUN DEBIAN_FRONTEND='noninteractive' TZ='Europe/London' apt-get update && apt-get install --assume-yes \
	tzdata keyboard-configuration

RUN apt-get update \
    && apt-get install --assume-yes --no-install-recommends \
        python3.8-dev python3-pip libboost-all-dev cmake \
        g++ build-essential \
    && rm -rf /var/lib/apt/lists/*

USER gitpod
